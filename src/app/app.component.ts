import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CoursAJ';
  animals = [
    {
      nom: 'Chien',
      img: 'https://www.notretemps.com/cache/com_zoo_images/c9/nom-chiot_6d24a44af72d79896fcaf95494afbff4.jpg',
      desc: 'C\'est un truc qui te suit partout'
    },
    {
      nom: 'Chat',
      img: 'https://geo.img.pmdstatic.net/fit/https.3A.2F.2Fwww.2Eneonmag.2Efr.2Fcontent.2Fuploads.2F2019.2F11.2Fistock-599117454.2Ejpg/1162x554/quality/80/background-color/ffffff/background-alpha/100/focus-point/1060%2C707/chat-langue-chaton.jpg',
      desc: 'C\'est un truc qui se branle de tout'
    }
  ];
  imgSelected = '';
  descSelected = '';
  nomSelected = '';
  imageSelected(item) {
    this.imgSelected = item.img;
    this.descSelected = item.desc;
    this.nomSelected = item.nom;
  }
}
